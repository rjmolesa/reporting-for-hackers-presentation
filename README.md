# README #

# "Reporting for Hackers" or "Create Amazing Reports with asciidoc, vim, and git" or "Word Sucks for Reporting!" The multi-title is a nod to Rocky and Bullwinkle. It's not that I'm indecisive.

Reporting for Hackers is the vim-loving, terminal cowboy wannabee’s, wet dream. I will show you how you too can generate reports pleasing to even the most discriminating of metric-loving-pointy-haired-boss and executive types. By combining vim, git, asciidoc, and a few other tools, you too can generate nice looking, easily navigable, and portable reports in pdf, html, docbook, man page, and slidy. I think it’s pretty sick and I hope you do too.

# Long Version

    When I needed to generate reports, I died a little inside when I recalled all of the pain from college doing group work. Merging different output from different teammates and different version of M$ Word. How the indentation was never consistent. How the content didn't copy and paste neatly. How jacked up the fonts got. Etcetera.

    I began my search for a toolchain that would meet the following requirements.
        - The source document had to be plain text
        - It had to generate a pdf with navigable table of contents
        - It must handle images properly
        - Must maximize configurability and control for the user, me
        - Extra credit if I could also use it to generate presentations from the same source.

    The source document is a plain text file or files that can be managed in git (or any other VCS). With this you get all the benefits of decentralized version control. Think teams working on the same project. Since it is all plain text filesyou can easily edit it with the tools of your choice. You can also pipe output from other tools to your source document for inclusion into the final report. Or comment it out to keep it as part of your notes.

    From this plain text source file I will show you how to generate very nice looking reports that are a pleasure to read. They are great for communicating important information to upper management and other stakeholders.

    The toolchain was primarily put together with pentesting in mind. However, it should be suitable for anyone that loves plain text, console friendly, editing. I will give each attendee a set of templates and scripts that they can build upon, customize, and start generating nice looking reports.

# What's actually here?

    The contents of this respository are the source files used to create the slides for the presentation. For the actual templates that you can use, modify, fork, etc. can be found https://bitbucket.org/rjmolesa/reporting-for-hackers-templates/overview 
