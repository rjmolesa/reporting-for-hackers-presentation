# Start a new automator Service project and select "Run Shell Script". Then copy and paste the below into automator. Save it and assign it a keyboard shortcut. You'll see why later.

#!/bin/bash

ts=`date +%Y%m%d%H%M%S`
screencapture -i ~/images/$ts-image.png
echo "image::images/$ts-image.png[]" | pbcopy
