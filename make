#!/bin/bash

_pwd=`pwd`

asciidoctor -T ~/repos/asciidoctor-deck.js/templates/haml reporting_for_hackers.ad
cd ~/repos/decktape
/home/mojo/repos/decktape/bin/phantomjs decktape.js -s 1366x768  file:///home/mojo/projects/presentations/reporting_for_hackers/reporting_for_hackers.html $_pwd/reporting_for_hackers.pdf
cd $_pwd
atril -s reporting_for_hackers.pdf &