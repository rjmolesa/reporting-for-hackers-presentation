<?xml version="1.0" encoding='utf-8'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:db="http://docbook.org/ns/docbook" version="1.0">

  <xsl:output method="text"/>

  <xsl:param name="font.main" select="'Liberation Serif'"/>
  <xsl:param name="font.sans" select="'Liberation Sans'"/>
  <xsl:param name="font.mono" select="'Liberation Mono'"/>

  <xsl:param name="theme" select="'Antibes'"/>
  <xsl:param name="theme.colors" select="'seahorse'"/>

  <xsl:template match="/*">
    <xsl:text>\documentclass[xetex, german]{beamer}&#10;</xsl:text>
    <xsl:text>\usetheme{</xsl:text>
    <xsl:value-of select="$theme"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\usecolortheme{</xsl:text>
    <xsl:value-of select="$theme.colors"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:call-template name="packages"/>
    <xsl:apply-templates select="db:info"/>
    <xsl:text>\begin{document}&#10;</xsl:text>
    <xsl:text>\maketitle&#10;</xsl:text>
    <xsl:apply-templates select="db:chapter|db:section"/>
    <xsl:text>\end{document}&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="packages">
    <xsl:text>
\usepackage[ngerman]{babel}
\usepackage{fontspec}
\usepackage{hyperref}
\usepackage{url}
\usepackage{xltxtra}
  </xsl:text>
    <xsl:text>\setmainfont{</xsl:text>
    <xsl:value-of select="$font.main"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\setsansfont{</xsl:text>
    <xsl:value-of select="$font.sans"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\setmonofont{</xsl:text>
    <xsl:value-of select="$font.mono"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\usepackage{cmap}&#10;</xsl:text>
    <xsl:text>\usepackage{graphicx}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="/*/db:info">
    <xsl:text>\title{</xsl:text>
    <xsl:value-of select="db:title"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\author{</xsl:text>
    <xsl:apply-templates select="db:author"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:text>\date{</xsl:text>
    <xsl:value-of select="db:date"/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:title" mode="frame">
    <xsl:text>\frametitle{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>
  <!-- <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="char">_</xsl:with-param>
      <xsl:with-param name="text" select="$link" />
    </xsl:call-template> -->
  <xsl:template match="db:link">
    <xsl:variable name="link">
      <xsl:value-of
        select="@*[namespace-uri()='http://www.w3.org/1999/xlink' and local-name()='href']"/>
    </xsl:variable>
    <xsl:variable name="desc">
      <xsl:value-of select="."/>
    </xsl:variable>
    <xsl:text>\href{</xsl:text>
    <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="char">_</xsl:with-param>
      <xsl:with-param name="text" select="$link"/>
    </xsl:call-template>
    <xsl:text>}{</xsl:text>
    <xsl:value-of select="$desc"/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:author">
    <xsl:value-of select="db:personname/db:firstname"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="db:personname/db:othername"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="db:personname/db:surname"/>
    <xsl:text> &lt;</xsl:text>
    <xsl:value-of select="db:email"/>
    <xsl:text>&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="/*/db:chapter|/*/db:section">
    <xsl:text>\begin{frame}</xsl:text>
    <xsl:if test=".//db:literallayout">
      <xsl:text>[fragile]</xsl:text>
    </xsl:if>
    <xsl:text>&#10;</xsl:text>
    <xsl:apply-templates select="db:info/db:title|db:title" mode="frame"/>
    <xsl:apply-templates select="*[name()!='info' and name()!='title']"/>
    <xsl:text>\end{frame}&#10;%%%%%%%%%%%&#10;&#10;</xsl:text>
  </xsl:template>

  <!-- 
<xsl:template match="/*/db:chapter[db:informalfigure]|/*/db:section[db:informalfigure]">
  <xsl:text>&#10;{&#10;</xsl:text>
  <xsl:apply-templates select="db:informalfigure//db:imagedata" mode="fullscreen.image" />
  <xsl:text>\begin{frame}[plain]
\end{frame}
}&#10;</xsl:text>
</xsl:template>
-->


  <xsl:template match="db:itemizedlist">
    <xsl:text>\begin{itemize}&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{itemize}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:itemizedlist[db:title]">
    <xsl:text>\textbf{</xsl:text>
    <xsl:value-of select="db:title"/>
    <xsl:text>}&#10;</xsl:text>

    <xsl:text>\begin{itemize}&#10;</xsl:text>
    <xsl:apply-templates select="db:listitem"/>
    <xsl:text>\end{itemize}&#10;</xsl:text>

  </xsl:template>


  <xsl:template match="db:orderedlist">
    <xsl:text>\begin{enumerate}&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{enumerate}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:variablelist">
    <xsl:text>\begin{description}&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{description}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:varlistentry">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:varlistentry/db:term">
    <xsl:text>\item[</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>] </xsl:text>
  </xsl:template>

  <xsl:template match="db:varlistentry/db:listitem">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:orderedlist/db:listitem|db:itemizedlist/db:listitem">
    <xsl:text>\item </xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:listitem/db:simpara">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="db:para|db:simpara">
    <xsl:apply-templates/>
    <xsl:text>&#10;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:formalpara/db:title">
    <xsl:text>\textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:formalpara">
    <xsl:apply-templates/>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:emphasis">
    <xsl:text>\emph{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="db:emphasis[@role='strong']">
    <xsl:text>\textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="db:literallayout">
    <xsl:if test="@role='small'">
      <xsl:text>{\tiny&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\begin{verbatim}&#10;</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>&#10;\end{verbatim}&#10;</xsl:text>
    <xsl:if test="@role='small'">
      <xsl:text>}&#10;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="db:literal">
    <xsl:text>\texttt{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>


  <xsl:template match="db:informalfigure">
    <xsl:apply-templates select=".//db:imagedata"/>
  </xsl:template>

  <xsl:template match="db:figure//db:imagedata">
    <xsl:text>\begin{figure}&#10;</xsl:text>
    <xsl:text>\includegraphics[width=</xsl:text>
      <xsl:if test="current()/@contentwidth">
      <xsl:value-of select="number(substring-before(current()/@contentwidth, '%')) div 100" />
    </xsl:if>
    <xsl:text>\textwidth,keepaspectratio=true]{</xsl:text>
    <!-- <xsl:value-of select="@fileref" /> -->
    <xsl:value-of select="substring-before(.//@fileref,'.')"/>
    <xsl:text>}&#10;</xsl:text>
    <xsl:if test="parent::node()/parent::node()//db:textobject">
      <xsl:text>\caption{</xsl:text>
      <xsl:call-template name="replace_latex_chars">
        <xsl:with-param name="text"><xsl:value-of select="parent::node()/parent::node()//db:textobject/db:phrase/text()"/>        </xsl:with-param>
      </xsl:call-template>
      <xsl:text>}&#10;</xsl:text>
    </xsl:if>
    <xsl:text>\end{figure}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:figure//db:textobject"/>

  <xsl:template match="db:informalfigure//db:imagedata">
    <xsl:text>\includegraphics[width=</xsl:text>
    <xsl:if test="current()/@contentwidth">
      <xsl:value-of select="number(substring-before(current()/@contentwidth, '%')) div 100" />
    </xsl:if>
    <xsl:text>\textwidth,keepaspectratio=true]{</xsl:text>
    <!-- <xsl:value-of select="@fileref" /> -->
    <xsl:value-of select="substring-before(.//@fileref,'.')"/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:inlinemediaobject//db:imagedata">
    <xsl:text>\includegraphics[width=</xsl:text>
    <xsl:if test="current()/@contentwidth">
      <xsl:value-of select="number(substring-before(current()/@contentwidth, '%')) div 100" />
    </xsl:if>
    <xsl:text>\textwidth,keepaspectratio=true]{</xsl:text>
    <!-- <xsl:value-of select="@fileref" /> -->
    <xsl:value-of select="substring-before(.//@fileref,'.')"/>

    <xsl:text>}&#10;</xsl:text>
  </xsl:template>


  <xsl:template match="db:informalfigure//db:imagedata" mode="fullscreen.image">
    <xsl:text>\usebackgroundtemplate{\includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{</xsl:text>
    <!-- <xsl:value-of select="@fileref" /> -->
    <xsl:value-of select="substring-before(.//@fileref,'.')"/>

    <xsl:text>}}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:tip">
    <xsl:text>\begin{block}{Hinweis}\tiny&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{block}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:warning">
    <xsl:text>\begin{alertblock}{Achtung}\tiny&#10;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{alertblock}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:title" mode="table">
    <xsl:text>\caption{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:footnote/db:simpara">
    <xsl:text>\footnote{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="db:table">
    <xsl:text>\begin{table}[ht]&#10;</xsl:text>
    <xsl:apply-templates select="db:title" mode="table"/>
    <xsl:text>\begin{tabular}{</xsl:text>
    <xsl:for-each select=".//db:colspec">
      <xsl:text>c </xsl:text>
    </xsl:for-each>
    <xsl:text>}&#10;</xsl:text>
    <xsl:for-each select=".//db:row">
      <xsl:for-each select=".//db:entry">
        <xsl:apply-templates/>
        <xsl:if test="position() != last()">
          <xsl:text> &amp; </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:text> \\&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{tabular}</xsl:text>
    <xsl:text>\end{table}</xsl:text>
  </xsl:template>
  
  <xsl:template match="db:informaltable">
    <xsl:text>\begin{tabular}{</xsl:text>
    <xsl:for-each select=".//db:colspec">
      <xsl:text>c </xsl:text>
    </xsl:for-each>
    <xsl:text>}&#10;</xsl:text>
    <xsl:for-each select=".//db:row">
      <xsl:for-each select=".//db:entry">
        <xsl:apply-templates/>
        <xsl:if test="position() != last()">
          <xsl:text> &amp; </xsl:text>
        </xsl:if>
      </xsl:for-each>
      <xsl:text> \\&#10;</xsl:text>
    </xsl:for-each>
    <xsl:text>\end{tabular}</xsl:text>
  </xsl:template>

  <xsl:template name="replace_char">
    <xsl:param name="text"/>
    <xsl:param name="char"/>
    <xsl:choose>
      <xsl:when test="contains($text, $char)">
        <xsl:message>text=<xsl:value-of select="$text"/></xsl:message>
        <xsl:value-of select="substring-before($text,$char)"/>
        <xsl:text>\</xsl:text>
        <xsl:value-of select="$char"/>
        <xsl:call-template name="replace_char">
          <xsl:with-param name="text">
            <xsl:value-of select="substring-after($text,$char)"/>
          </xsl:with-param>
          <xsl:with-param name="char" select="$char"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="replace_latex_chars">
    <xsl:param name="text"/>
    <xsl:call-template name="replace_char">
      <xsl:with-param name="char">_</xsl:with-param>
      <xsl:with-param name="text">
        <xsl:call-template name="replace_char">
          <xsl:with-param name="char">%</xsl:with-param>
          <xsl:with-param name="text">
            <xsl:call-template name="replace_char">
              <xsl:with-param name="char">&amp;</xsl:with-param>
              <xsl:with-param name="text">
                
        <xsl:call-template name="replace_char">
      <xsl:with-param name="char">#</xsl:with-param>
      <xsl:with-param name="text" select="$text" />
    </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="text()[contains(., '_')]">
    <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="text()[contains(., '&amp;')]">
    <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="text()[contains(., '%')]">
    <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="text()[contains(., '#')]">
    <xsl:call-template name="replace_latex_chars">
      <xsl:with-param name="text">
        <xsl:value-of select="."/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>


</xsl:stylesheet>
